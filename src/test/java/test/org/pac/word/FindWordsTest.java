package test.org.pac.word;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.pac.word.CharLine;
import org.pac.word.FindWords;
class FindWordsTest {


	private static char[][] charMat1 = {
			{'a', 'b', 'c', 'd', 'e', 'f'}, 
            {'g', 'h', 'i', 'j', 'k', 'l'}, 
            {'m', 'n', 'o', 'p', 'q', 'r'},
            {'s', 't', 'u', 'v', 'w', 'x'},
            {'y', 'z', 'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h', 'i', 'j'}
            };
	private static char[][] charMat2 = {
			{'a', 'b', 'c', 'd', 'e', 'f'}, 
            {'g', 'h', 'i', 'j', 'k', 'l'}, 
            {'m', 'n', 'o', 'p', 'q', 'r'},
            {'s', 't', 'u', 'v', 'w', 'x'},
            {'y', 'z', 'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h', 'i', 'j'}
            };
	private static char[][] charMat3 = {
			{'a', 'b', 'c', 'd', 'e', 'f'}, 
            {'g', 'h', 'i', 'j', 'k', 'l'}, 
            {'m', 'n', 'o', 'p', 'q', 'r'},
            {'s', 't', 'u', 'v', 'w', 'x'},
            {'y', 'z', 'a', 'b', 'c', 'd'},
            {'e', 'f', 'g', 'h', 'i', 'j'}
            };
	private static char[][] charMat2by8 = {
			{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'},
			{'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'}
            };
	private static char[][] charMat8by2 = {
			{'a', 'b'},
			{'c', 'd'},
			{'e', 'f'}, 
            {'g', 'h'},
            {'i', 'j'},
			{'k', 'l'}, 
            {'m', 'n'},
            {'o', 'p'}
            };
	@Test
	void testCreateRows() throws Exception {
		
		List<CharLine> rows = null;
		rows=FindWords.createRows(charMat1,6,6);
		   assertEquals( rows.get(0).getLine(), "abcdef");
		
	}

	@Test
	void testCreateCols() throws Exception {
		
		List<CharLine> cols = null;
	
		cols= FindWords.createCols(charMat1,6,6);
        assertEquals( cols.get(0).getLine(), "agmsye");

		
	}
	@Test
	void testCreateCols2by8() throws Exception {
		
		List<CharLine> cols = null;
	
		cols= FindWords.createCols(charMat2by8,2,8);
        assertEquals( cols.get(0).getLine(), "ak");

		
	}
	@Test
	void testCreateCols8by2() throws Exception {
	
		List<CharLine> cols = null;
	
		cols= FindWords.createCols(charMat8by2,8,2);
        assertEquals( cols.get(0).getLine(), "acegikmo");	
	}
	
	@Test
	void testCreateColsRev8by2() throws Exception {
		
		List<CharLine> cols = null;
	
		cols= FindWords.createCols(charMat8by2,8,2);
        assertEquals( cols.get(0).getReverseline(), "omkigeca");	
	}
	@Test
	void testCreateRightDiag() throws Exception {
		
		List<CharLine> rtDiags = null;

		rtDiags = FindWords.createRightDiag(charMat1,6,6);
		 assertEquals( rtDiags.get(0).getReverseline(), "fy");
		 assertEquals( rtDiags.get(0).getLine(), "yf");

		
	}

	@Test
	void testCreateLeftDiag() throws Exception {
		
		List<CharLine> lfDiags = null;
		lfDiags = FindWords.createLeftDiag(charMat1,6,6);
		 assertEquals( lfDiags.get(0).getReverseline(), "id");
		
	}

}
