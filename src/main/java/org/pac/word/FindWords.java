package org.pac.word;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Peter Coyle
 *
 */
public class FindWords {


	private static final String NOT_FOUND ="Not found";
	public static void main(String[] args) {

		char[][] charMat;
		int numCols = 0;
		int numRows = 0;
		List<CharLine> combinedList = null; 
		List<String> inputLines = null;
		List<String> targetList = null;
		String rowAndColStrs[]=null;
		String result;

		try {

			inputLines = init("testInput-1.txt");
			String line1;
			line1=inputLines.get(0);
			rowAndColStrs=line1.split("x");
			numRows=Integer.parseInt(rowAndColStrs[0]);  
			numCols=Integer.parseInt(rowAndColStrs[1]); 
			//       	 System.out.println("rows: "+numRows+" cols: "+numCols); 
			//    	 System.out.println("rows first line "+line1);
			charMat = createCharMatrix(inputLines, numRows, numCols);
			targetList = buildTargetList(inputLines, numRows);
			//	System.out.println("done.");

			combinedList = createCombinedList(charMat,numRows, numCols);

			for (int i=0; i< targetList.size(); i++) {
				String targetWord=targetList.get(i);
				result = findWord(combinedList, targetWord);
				System.out.println(result);
			}


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * @param combinedList
	 * @param targetWord
	 * @return
	 */
	private static String findWord(List<CharLine> combinedList, String targetWord) {
		CharLine mycharline;
		String resultStr=null;

		for (int j=0; j< combinedList.size(); j++) {
			mycharline = combinedList.get(j);
			resultStr = mycharline.searchLine(targetWord);
			if (!resultStr.equals(NOT_FOUND)) {
				//  System.out.println("Result: "+resultStr);
				break;
			}
		}
		return resultStr;

	}

	/**
	 * @param charMat - The character matrix
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return - A list of all the possible strings in the character matrix (row, col, left diag, right diag AND the reverse of each one)
	 * 
	 * First we build all the strings, by constructing the 4 possible ways a word can be found:
	 * Rows
	 * Cols
	 * Right diagonals
	 * Left diagonals
	 * Then append them all to one master list to return
	 */
	private static List<CharLine> createCombinedList(char[][] charMat, int numRows, int numCols) {
	
		List<CharLine> rows = null;
		List<CharLine> cols = null;
		List<CharLine> rtDiags = null;
		List<CharLine> lfDiags = null;
		List<CharLine> returnList = null; 

		returnList = new ArrayList<>(); 

		try {
			
			rows=createRows(charMat,numRows,numCols);
			cols=createCols(charMat, numRows,numCols);
			rtDiags = createRightDiag(charMat, numRows, numCols);
			lfDiags = createLeftDiag(charMat, numRows, numCols);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		returnList.addAll(rows);
		returnList.addAll(cols);
		returnList.addAll(rtDiags);
		returnList.addAll(lfDiags);

		return returnList;
	}



	/**
	 * @param inputLines - The list containing the entire input file
	 * @param numRows - Number of rows in the matrix (needed to know how many lines to skip)
	 * @return - The list of words to be found
	 */
	private static List<String> buildTargetList(List<String> inputLines, int numRows) {
		List<String> returnList = new ArrayList<>(); 
		String targetStr;

		for (int i=(numRows+1); i< inputLines.size(); i++) {
			targetStr = inputLines.get(i);
			targetStr= targetStr.replaceAll("\\s", "");	 
			returnList.add(targetStr);
		}
		return returnList;
	}

	/**
	 * @param charMat - The character matrix
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return - A list of type CharLine containing the string from each row
	 * @throws Exception
	 */
	public static List<CharLine> createRows(char[][] charMat, int nRows, int nCols) throws Exception {
		String strRow = "";
		CharLine charline = null;
		int locIndex =0;
		List<CharLine> rows = new ArrayList<>(); 

		for (int i=0;i<nRows;i++) {
			charline = new CharLine();
			charline.locations = new MatLoc[nCols];
			locIndex =0;
			strRow="";
			for (int j=0; j<nCols; j++) {
				strRow=strRow+charMat[i][j];
				charline.locations[locIndex]=new MatLoc(i,j);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(nCols);
			rows.add(charline);
		}
		return rows;
	}

	/**
	 * @param charMat - The character matrix
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return A list of type CharLine containing the string from each col.
	 * @throws Exception
	 */
	public static List<CharLine> createCols(char[][] charMat, int nRows, int nCols) throws Exception {
		String strRow = "";
		CharLine charline = null;
		int locIndex =0;
		List<CharLine> charlinelist = new ArrayList<>(); 

		for (int j=0; j< nCols; j++) {

			charline = new CharLine();
			charline.locations = new MatLoc[nRows];
			locIndex =0;
			strRow="";
			for (int i=0;i<nRows;i++) {
				strRow=strRow+charMat[i][j];
				charline.locations[locIndex]=new MatLoc(i,j);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(nRows);
			charlinelist.add(charline);
		}
		return charlinelist;
	}


	/**
	 * @param charMat - The character matrix
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return A list of type CharLine containing the string from each right diag
	 * @throws Exception
	 */
	public static List<CharLine> createRightDiag(char[][] charMat, int nRows, int nCols) throws Exception {
		String strRow = "";
		CharLine charline = null;
		int locIndex =0;
		List<CharLine> charlinelist = new ArrayList<>(); 
		//int 
		for (int j=nRows-2; j>=0; j--) {

			charline = new CharLine();
			charline.locations = new MatLoc[Math.max(nCols+1, nRows+1)];
			locIndex =0;
			strRow="";
			// while
			for (int i=j,  k=0;i<nRows && k<=nCols;i++,k++) {
				strRow=strRow+charMat[i][k];
				charline.locations[locIndex]=new MatLoc(i,k);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(strRow.length());
			charlinelist.add(charline);
		}
		for (int j=1; j< (nCols-1); j++) {
			charline = new CharLine();
			charline.locations = new MatLoc[Math.max(nCols+1, nRows+1)];
			locIndex =0;
			strRow="";
			for (int i=0,  k=j;i<nRows && k<nCols;i++,k++) {
				strRow=strRow+charMat[i][k];
				charline.locations[locIndex]=new MatLoc(i,k);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(strRow.length());
			charlinelist.add(charline);

		}
		return charlinelist;
	}

	/**
	 * @param charMat - The character matrix
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return A list of type CharLine containing the string from each left diag
	 * @throws Exception
	 */
	public static List<CharLine> createLeftDiag(char[][] charMat, int nRows, int nCols) throws Exception {
		String strRow = "";
		CharLine charline = null;
		int locIndex =0;
		List<CharLine> charlinelist = new ArrayList<>(); 
		for (int j=nRows-2; j>=0; j--) {

			charline = new CharLine();
			charline.locations = new MatLoc[Math.max(nCols+1, nRows+1)];
			locIndex =0;
			strRow="";
			// while
			for (int i=j,  k=nCols-1; i<nRows  && k>=0;i++,k--) {
				strRow=strRow+charMat[i][k];
				charline.locations[locIndex]=new MatLoc(i,k);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(strRow.length());
			charlinelist.add(charline);
		}
		for (int j=nCols-2; j>0; j--) {
			charline = new CharLine();
			charline.locations = new MatLoc[Math.max(nCols+1, nRows+1)];
			locIndex =0;
			strRow="";
			for (int i=0,  k=j;i<nRows && k>=0;i++,k--) {
				strRow=strRow+charMat[i][k];
				charline.locations[locIndex]=new MatLoc(i,k);
				locIndex++;
			}
			charline.line=strRow;
			charline.reverseline = new StringBuilder(strRow).reverse().toString();
			charline.setrevlocations(strRow.length());
			charlinelist.add(charline);
		}

		return charlinelist;
	}


	/**
	 * @param inputfilename
	 * @return A list of string (one for each line in the file
	 */
	static List<String> init(String inputfilename) {

		List<String> inputLines = null;
		inputLines = new ArrayList<>(); 
		try (BufferedReader br = new BufferedReader(new FileReader(inputfilename)))
		{
			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				inputLines.add(currentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} 
		return inputLines;
	}
	/**
	 * @param inputLines - All the lines from the input files
	 * @param numRows - Number of rows in the character matrix
	 * @param numCols - Number of columns in the character matrix
	 * @return - Character matrix
	 */
	private static char[][] createCharMatrix(List<String> inputLines, int numRows, int numCols) {
		char[][] myCharMat;
		myCharMat = new char[numRows][numCols];
		String rowStr = null;
		for (int i=1; i<=numRows;i++) {
			rowStr = inputLines.get(i);
			rowStr= rowStr.replaceAll("\\s", "");	 
			for (int j=0; j<numCols;j++) {
				myCharMat[i-1][j]= rowStr.charAt(j);
			}
		}
	
		return myCharMat;
	}

}

